<?php
/**
 * @file
 * Page callbacks
 */

/**
 * Callback function for file/download/%file menu.
 */
function webform_gated_file_webform_page($file, $webform) {
  global $conf;

  // Disable cache for this page. One alternative is setting http header:
  // drupal_add_http_header('Cache-Control', 'public, max-age=0');
  $conf['cache'] = '0';

  $setting = webform_gated_file_load_setting($file->fid);

  if (!$setting) {
    drupal_goto(file_create_url($file->original_uri));
  }

  $webform_nid = webform_gated_file_get_protection_webform_nid($setting);
  $download_activation_key = _webform_gated_file_get_download_activation_cookie_key($file, $webform);

  if ((webform_gated_file_is_form_submitted($file, $webform) || $webform_nid != $webform->nid) && isset($_COOKIE[$download_activation_key])) {
    // If the form is submitted, the request timestamp should have been saved.
    $timestamp = _webform_gated_file_get_download_request_time($file, $webform);
    drupal_goto(webform_gated_file_get_file_download_url($file, $webform, $timestamp));
  }

  drupal_set_title($webform->title);

  setcookie('fid', $file->fid, 0, '/', '', FALSE, TRUE);
  $entities = entity_load($setting['entity_type'], array($setting['entity_id']));

  return theme('gated_file_webform', array(
    'webform' => $webform,
    'file' => $file,
    'entity' => reset($entities),
    'setting' => $setting,
  ));
}


/**
 * Callback for downloading a file.
 */
function webform_gated_file_download_file($file, $webform) {
  global $conf;

  // Disable cache for this page. One alternative is setting http header:
  // drupal_add_http_header('Cache-Control', 'public, max-age=0');
  $conf['cache'] = '0';
  $real_uri = webform_gated_file_get_real_uri($file);
  $scheme = file_uri_scheme($real_uri);

  if (file_stream_wrapper_valid_scheme($scheme) && file_exists($real_uri)) {
    $headers = array('Content-Type' => $file->filemime);

    _webform_gated_file_ensure_download_once($file, $webform);
    _webform_gated_file_update_download_info($file, $webform);
    _webform_gated_file_mark_download_activation($file, $webform);

    file_transfer($real_uri, $headers);
  }
  else {
    drupal_not_found();
  }
}


/**
 * Update the component list select box on the form.
 */
function webform_gated_file_webform_components($webform) {
  $components = _webform_gated_file_get_webform_components($webform);
  $options = '';

  foreach ($components as $id => $name) {
    $options .= '<option value="' . $id . '">' . $name . '</option>';
  }

  die($options);
}
