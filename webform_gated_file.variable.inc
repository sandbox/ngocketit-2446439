<?php
/**
 * @file
 * Variable declarations
 */

/**
 * Implements hook_variable_info().
 */
function webform_gated_file_variable_info($options) {
  $variables['webform_gated_file_email_subject'] = array(
    'type' => 'string',
    'title' => t('Email subject', array(), $options),
    'default' => 'Your file download link',
    'description' => t('Subject of the email sent to the users', array(), $options),
  );

  $variables['webform_gated_file_email_body'] = array(
    'type' => 'string',
    'title' => t('Email message', array(), $options),
    'default' => 'Please follow this link: !url to download your file',
    'description' => t('Body of the email sent to the users', array(), $options),
  );

  return $variables;
}
