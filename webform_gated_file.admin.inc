<?php
/**
 * @file
 * Contains features for admin
 */

function webform_gated_file_settings_form($form, &$form_state) {
  $options = array();
  $types = entity_get_info();

  $form['webform_gated_file_enabled_entity_types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Protected entity types'),
    '#collapsible' => TRUE,
    '#tree' => TRUE,
  );

  $enabled_types = variable_get('webform_gated_file_enabled_entity_types', array());

  foreach ($types as $type_name => $type_info) {
    if (!$type_info['fieldable']) {
      continue;
    }

    $enabled_bundles = array_values($enabled_types[$type_name]['bundles']);

    $form['webform_gated_file_enabled_entity_types'][$type_name] = array(
      '#type' => 'fieldset',
      '#title' => check_plain($type_info['label']),
      '#collapsible' => TRUE,
      '#collapsed' => count(array_unique($enabled_bundles)) <= 1,
    );

    $bundles = field_info_bundles($type_name);
    $options = array();

    foreach ($bundles as $bundle_name => $bundle_info) {
      $options[$bundle_name] = $bundle_info['label'];
    }

    $form['webform_gated_file_enabled_entity_types'][$type_name]['bundles'] = array(
      '#type' => 'checkboxes',
      '#options' => $options,
      '#default_value' => !empty($enabled_types) ? $enabled_types[$type_name]['bundles'] : array(),
    );
  }

  $webform_types = webform_variable_get('webform_node_types');
  $webform_options = array();
  foreach ($webform_types as $type) {
    $webform_options[$type] = $type;
  }

  $form['webform'] = array(
    '#type' => 'fieldset',
    '#title' => t('Webform'),
    '#collapsible' => TRUE,
  );

  $form['roles'] = array(
    '#type' => 'fieldset',
    '#title' => t('User roles'),
    '#collapsible' => TRUE,
  );

  $form['roles']['webform_gated_file_enabled_roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('User roles that the protection applies to'),
    '#description' => t('Only users with selected roles will have the protection'),
    '#options' => user_roles(),
    '#default_value' => variable_get('webform_gated_file_enabled_roles', array(1 => '1', 2 => '2')),
  );

  $form['file_schemes'] = array(
    '#type' => 'fieldset',
    '#title' => t('File schemes'),
    '#collapsible' => TRUE,
  );

  $form['file_schemes']['webform_gated_file_enabled_file_schemes'] = array(
    '#type' => 'checkboxes',
    '#title' => t('File schemes that the protection applies to'),
    '#description' => t('Only files with selected schemes will be protected'),
    '#options' => array(
      'private' => t('Private'),
      'public' => t('Public'),
    ),
    '#default_value' => variable_get('webform_gated_file_enabled_file_schemes', array('private', 'public')),
  );

  $webform = node_load(variable_get('webform_gated_file_webform_nid', FALSE));

  $form['webform']['webform_gated_file_webform_nid_search'] = array(
    '#type' => 'textfield',
    '#title' => t('Default protection Webform'),
    '#description' => t('Enter title of the node for autocomplete search'),
    '#maxlength' => 255,
    '#size' => 80,
    '#autocomplete_path' => 'webform_gated_file/webform_autocomplete',
    '#default_value' => $webform ? (check_plain($webform->title) . " [$webform->nid]") : '',
  );

  $webform_components = _webform_gated_file_get_webform_components(variable_get('webform_gated_file_webform_nid', 0));

  $form['webform']['webform_gated_file_webform_email_component'] = array(
    '#type' => 'select',
    '#title' => t('Email component'),
    '#description' => t('The Webform component where email address of user will be extracted'),
    '#options' => $webform_components,
    '#default_value' => variable_get('webform_gated_file_webform_email_component', 0),
    '#validated' => TRUE,
  );

  $form['webform_gated_file_webform_nid'] = array(
    '#type' => 'hidden',
    '#value' => '',
  );

  $form['mail_sending'] = array(
    '#type' => 'fieldset',
    '#title' => t('Mail sending'),
    '#collapsible' => TRUE,
  );

  $form['mail_sending']['webform_gated_file_verify_email'] = array(
    '#type' => 'checkbox',
    '#title' => t('Verify user\'s email address'),
    '#description' => t('If set, the download link will be sent to user\'s email address. Otherwise, they are prompted for download immediately after submission'),
    '#default_value' => variable_get('webform_gated_file_verify_email', TRUE),
  );

  $form['mail_sending']['webform_gated_file_email_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Email subject'),
    '#dscription' => t('Subject of the email sent to user'),
    '#default_value' => variable_get_value('webform_gated_file_email_subject'),
    '#states' => array(
      'visible' => array(
        ':input[name="webform_gated_file_verify_email"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['mail_sending']['webform_gated_file_email_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Email message'),
    '#dscription' => t('Body of the email sent to user'),
    '#default_value' => variable_get_value('webform_gated_file_email_body'),
    '#states' => array(
      'visible' => array(
        ':input[name="webform_gated_file_verify_email"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['mail_sending']['tokens'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => t('Available tokens'),
  );

  $email_tokens = webform_gated_file_get_email_tokens();
  $tokens = array();

  foreach ($email_tokens as $key => $info) {
    $tokens[] = check_plain("!{$key} == {$info['description']}");
  }

  $form['mail_sending']['tokens']['items'] = array(
    '#theme' => 'item_list',
    '#items' => $tokens,
    '#type' => 'ul',
  );

  $form['submission'] = array(
    '#type' => 'fieldset',
    '#title' => t('Submission'),
    '#collapsible' => TRUE,
  );

  $form['submission']['webform_gated_file_remember_submission'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use cookie to remember the form submission state'),
    '#description' => t('If set, users won\'t be prompted to refill the form if they have already done that'),
    '#default_value' => variable_get('webform_gated_file_remember_submission', TRUE),
  );

  $form['submission']['webform_gated_file_remember_period'] = array(
    '#type' => 'textfield',
    '#title' => t('Remember period'),
    '#size' => 10,
    '#description' => t('Specify the number of days the the submission is remembered before users have to submit the form again.<br/>Enter zero (0) to make the submission valid until users close the browser.'),
    '#default_value' => variable_get('webform_gated_file_remember_period', 1),
    '#states' => array(
      'visible' => array(
        ':input[name="webform_gated_file_remember_submission"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['submission']['webform_gated_file_cookie_salt'] = array(
    '#type' => 'textfield',
    '#title' => t('Submission remember cookie salt'),
    '#size' => 40,
    '#description' => t('The salt used to calculate cookie which remembers whether user has submitted the form. If this value is reset, all the current remember cookies will become invalid and users will have to refill the form. It\'s recommended that you change this to a different value than the default one.'),
    '#default_value' => variable_get('webform_gated_file_cookie_salt', 'Ngoc<3Van'),
    '#states' => array(
      'visible' => array(
        ':input[name="webform_gated_file_remember_submission"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['submission']['webform_gated_file_download_hash_salt'] = array(
    '#type' => 'textfield',
    '#title' => t('File download hash salt'),
    '#size' => 40,
    '#description' => t('The salt used to calculate download hash. If this value is reset, all the current remember cookies will become invalid and users will have to refill the form. It\'s recommended that you change this to a different value than the default one.'),
    '#default_value' => variable_get('webform_gated_file_download_hash_salt', 'Bo<32MeCon'),
  );

  return system_settings_form($form);
}


/**
 * Validation handler for settings form.
 */
function webform_gated_file_settings_form_validate($form, &$form_state) {
  $webform = $form_state['values']['webform_gated_file_webform_nid_search'];
  $matches = array();

  if (empty($webform)) {
    return;
  }

  // Are we creating a new disclaimer?.
  if (!preg_match('/\[([0-9]+)\]$/', $webform, $matches)) {
    form_set_error('webform_gated_file_webform_nid_search', t('Please check & make sure that the node title is entered correctly'));
  }
  else {
    // Then make sure that the node is existing and accessible.
    $node_id = $matches[1];
    if (!($node = node_load($node_id)) || $node->status != NODE_PUBLISHED) {
      form_set_error('webform_gated_file_webform_nid_search', t('Node with ID %nid can not be found.', array('%nid' => $node_id)));
    }

    $form_state['values']['webform_gated_file_webform_nid'] = $node_id;
  }

  $remember_period = intval($form_state['values']['webform_gated_file_remember_period']);
  if ($remember_period < 0) {
    form_set_error('webform_gated_file_remember_period', t('Remember time period can not be negative.'));
  }
}

/**
 * Auto complete search for webform node.
 */
function webform_gated_file_webform_autocomplete_search($string = '') {
  $webform_types = webform_variable_get('webform_node_types');

  if (empty($string) || empty($webform_types)) {
    return;
  }

  $matches = array();

  $query = db_select('node')
    ->fields('node', array('nid', 'title'))
    ->condition('status', NODE_PUBLISHED)
    ->condition('title', db_like($string) . '%', 'LIKE')
    ->condition('type', $webform_types, 'IN');

  $result = $query->execute();

  foreach ($result as $node) {
    $matches[$node->title . " [$node->nid]"] = check_plain($node->title);
  }

  return drupal_json_output($matches);
}
